OVERVIEW
--------

Provides fields from the XML sitemap module to Views.

FEATURES
--------

Specifically, it allows you to display, sort and filter on these four fields:

* Status/Inclusion: Is this included in the sitemap?
* Status Override: Has the default status been overridden?
* Priority: The priority (0 - 1).
* Priority Override: Has the default priority been overridden?

REQUIREMENTS
------------

XML Sitemap https://drupal.org/project/xmlsitemap
Views       https://drupal.org/project/views
